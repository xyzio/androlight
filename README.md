AndroLight
==========

Open source flashlight app for Android.

Available on the play store at [https://play.google.com/store/apps/details?id=com.xyzio.androlight](https://play.google.com/store/apps/details?id=com.xyzio.androlight)
