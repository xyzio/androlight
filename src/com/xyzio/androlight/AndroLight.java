package com.xyzio.androlight;

import java.util.ArrayList;
import java.util.Random;

import com.xyzio.androlight.PreferencesActivity;
import com.xyzio.androlight.R;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class AndroLight extends Activity {

	SeekBar seekbar;
	
	private Handler mHandler = new Handler();
	private Handler fHandler = new Handler();
	
	int strobeCounter = 0;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        setContentView(R.layout.activity_andro_light);
        
        InitSeekbar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	
        getMenuInflater().inflate(R.menu.activity_andro_light, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
	@Override
	public void onResume() {
		super.onResume();
		SetBackgroundColor();
		StrobeSetup();
		FlickerSetup();
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	super.onOptionsItemSelected(item);
    	
    	switch(item.getItemId()) {
    	case R.id.menu_settings:
    		startActivity(new Intent(this, PreferencesActivity.class));
    		return true;
    	}
    	return false;
    }
    
	public void StrobeSetup() {
		
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean pref_disco = sharedPref.getBoolean("pref_disco", false);
		mHandler.removeCallbacks(mUpdateStrobeTask);
		
		if (true == pref_disco) {
			mHandler.postDelayed(mUpdateStrobeTask, 1000);	
		}
	}
	
	public void FlickerSetup() {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean pref_flicker = sharedPref.getBoolean("pref_flicker", false);
		
		fHandler.removeCallbacks(mUpdateFlickerTask);
		
		if (true == pref_flicker) {
			fHandler.postDelayed(mUpdateFlickerTask, 1000);
		}
	}
	
	private Runnable mUpdateFlickerTask = new Runnable() {
		
		public void run() {
			
			Random rand = new Random();
			
        	WindowManager.LayoutParams params = getWindow().getAttributes();
        	float myFloat = rand.nextInt(99);
        	if (myFloat < 10) 
        		myFloat = 10;
        	
        	myFloat = myFloat / 100;
        	params.screenBrightness = myFloat;
        	getWindow().setAttributes(params);
        	
        	fHandler.postAtTime(this, SystemClock.uptimeMillis() + 1000);
		}
	};

	private Runnable mUpdateStrobeTask = new Runnable() {
	
		public void run() {
	
			ArrayList<Integer> colorList = new ArrayList<Integer>();		   
			colorList.add(android.R.color.holo_green_dark);
			colorList.add(android.R.color.white);
			colorList.add(android.R.color.holo_orange_dark);
			colorList.add(android.R.color.black);
			colorList.add(android.R.color.holo_red_dark);		   
			
			int index = strobeCounter++ % colorList.size();
			
			View someView = findViewById(R.id.textview_title);
			View root = someView.getRootView();
			root.setBackgroundColor(getResources().getColor((colorList.get(index))));    
			mHandler.postAtTime(this, SystemClock.uptimeMillis() + 1000);
		}
	};

    
    
	public void SetBackgroundColor() {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		String backgroundColor = sharedPref.getString("pref_backgroundColor", "");
	
		int color = android.R.color.holo_orange_dark;
		if (backgroundColor.toLowerCase().contains("green") == true) {
			color = android.R.color.holo_green_dark;
		}
		else if (backgroundColor.toLowerCase().contains("white") == true) {
			color = android.R.color.white;
		}
		else if (backgroundColor.toLowerCase().contains("orange") == true) {
			color = android.R.color.holo_orange_dark;
		}
		else if (backgroundColor.toLowerCase().contains("black") == true) {
			color = android.R.color.black;
		}
		else if (backgroundColor.toLowerCase().contains("red") == true) {
			color = android.R.color.holo_red_dark;
		}
        
        View someView = findViewById(R.id.seekbar_brightness);
        View root = someView.getRootView();
        root.setBackgroundColor(getResources().getColor((color)));
	}
	
	
	public void InitSeekbar() {
		seekbar = (SeekBar) findViewById(R.id.seekbar_brightness);
		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
	        //params.flags |= LayoutParams.FLAG_KEEP_SCREEN_ON;
	        if ((progress > 10)) {
	        	WindowManager.LayoutParams params = getWindow().getAttributes();
	        	float myFloat = progress;
	        	myFloat = myFloat / 100;
	        	params.screenBrightness = myFloat;
	        	getWindow().setAttributes(params);
	        }
		}

		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
		}

		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
		}		
		});
	}
    
}
